%% LaTeX Beamer presentation template (requires beamer package)
%% see http://bitbucket.org/rivanvx/beamer/wiki/Home
%% idea contributed by H. Turgut Uyar
%% template based on a template by Till Tantau
%% this template is still evolving - it might differ in future releases!

\documentclass{beamer}

\mode<presentation>
{
\usetheme{Hannover}

\setbeamercovered{transparent}
} 

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}

% font definitions, try \usepackage{ae} instead of the following
% three lines if you don't like this look
\usepackage{mathptmx}
\usepackage[scaled=.80]{helvet}
\usepackage{courier}
\usepackage{graphicx}
\usepackage{subfigure}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}


\usepackage[T1]{fontenc}


\title{Taking Multi-Object Tracking to the Next Level: People, Unknown Objects, and Carried Items}

%\subtitle{}

% - Use the \inst{?} command only if the authors have different
%   affiliation.
%\author{F.~Author\inst{1} \and S.~Another\inst{2}}
\author{Review by Igor Bogoslavskyi}

% - Use the \inst command only if there are several affiliations.
% - Keep it simple, no one is interested in your street address.
\institute[Universities of]
{
Department Of Computer Science\\
Albert Ludwigs University Freiburg\\
email: bogoslai@informatik.uni-freiburg.de}

\date{17.01.2013}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Outline}
\tableofcontents
% You might wish to add the option [pausesections]
\end{frame}


\section{Motivation}
%slide 1
\begin{frame}
\frametitle{Motivation} 
\begin{itemize}
  \item Tracking objects in the moving scene is an important task in mobile robotics
\end{itemize}

\begin{center}
  \includegraphics[width=8cm]{image-intro.jpg}
\end{center}

\end{frame}

% slide 2
\begin{frame}
\frametitle{} 
\begin{itemize}
  \item Previously only \emph{tracking-by-detection} approaches were used. These, however, need pre-trained detector models.
  \item To understand the behavior of people, it is important to recognize and track other objects in their surroundings.
  \item Therefore methods that can detect and track also novel object types and learn models for them online are needed.
\end{itemize}
\begin{center}
  \includegraphics[width=7.5cm]{motivation_items.jpg}
\end{center}
\end{frame}

\section{Problem Description}
%slide 3
\begin{frame}
\frametitle{Problem Description} 
\begin{itemize}
  \item The problem of detecting novel objects is not trivial.
  \item To do that one has to answer an even harder question - \emph{what is an object}.
  \item This itself involves segmenting object from the video stream input.
\end{itemize}
\end{frame}


\section{Tracking-Before-Detection Approach}
\subsection{Overview}
%slide 4
\begin{frame}
\frametitle{Tracking-Before-Detection Approach Overview} 
\begin{itemize}
  \item Noisy stereo estimates are used to extract regions-of-interest (ROIs) in the input images and to segment them into candidate objects.
  \item Each ROI is then tracked independently in 3D.
  \item Each segmented object is then sent to an object classificator for classification into pedestrians and other objects.
\end{itemize}
\begin{center}
  	\includegraphics[width=10cm]{overview.pdf}
	\end{center}
\end{frame}

%slide 5
\subsection{ROI Extraction and Segmentation}
\begin{frame}
\frametitle{ROI Extraction and Segmentation} 

\begin{itemize}
  \item Given depth image generate a set of ROIs for potential objects.
  \begin{itemize}
  	\item The 3D points within 2m height corridor are projected onto the ground plane.
  	\item A 2D histogram of these points is taken
  	\item The histogram bins are thresholded for removing noise.
  	\item Resulting bins are grouped into connected components (image, top)
  \end{itemize}
  \item The connected components are found by segmenting a smoothed version of the original histogram via Quick Shift algorithm.  
\end{itemize}
	\begin{center}
  \includegraphics[width=5cm]{histogram.jpg}
  \end{center}
\end{frame}

%slide 6
\subsection{Quick Shift}
\begin{frame}
\frametitle{Quick Shift} 

\begin{itemize}
  \item Quick Shift finds the modes of density by shifting each point to the nearest neighbor with higher density value. 
  %Formally:
  %$$ y_i =  \underset{j:P_j>P_i}{\operatorname{argmax}} \hspace{1pt} d(x_i,x_j), \hspace{3pt} P_i = \frac{1}{N} \sum_{j=1}^{N} \theta(d(x_i,x_j))$$
  \item Quick Shift is performed for every point in smoothed histogram.
  \item As a result we get a segmentation of the ROIs into individual objects.
\end{itemize}
	\begin{center}
  \includegraphics[width=5cm]{image-045.jpg}
  \end{center}
\end{frame}

%slide 7
\subsection{Data Association}
\begin{frame}
\frametitle{Data Association} 

\begin{itemize}
  \item We need to associate ROIs with existing tracks.
  \item This is done by matching them to each track's ROI from previous frame.
  \item ROIs are assumed to match if the over-union of their ground projection footprints is over 50\%.
  \item We start a new track for all ROIs that cannot be associated.
\end{itemize}
\end{frame}

%slide 8
\subsection{ICP Tracking and Model Update}
\begin{frame}
\frametitle{ICP Tracking and Model Update} 

\begin{itemize}
  \item Adapted version of ICP is used for aligning the model from previous step with the 3D points from overlaping ROI.
  \begin{itemize}
  \item The red points in (a) are the 3D points from the GCT model.
  \item The blue points are the 3D points from the overlaping ROI.
  \item We obtain rotation and translation for moving the GCT model via ICP.
  \item The last step is to update the registered GCT model with new 3D points.
\end{itemize}
\end{itemize}
\begin{center}
  \includegraphics[width=4cm]{registration.jpg}
\end{center}
\end{frame}

%slide 9
\subsection{Object Classification}
\begin{frame}
\frametitle{Object Classification and Tracking} 

\begin{itemize}
  \item When the newly generated track comes into detection range, the ROI is passed to pedestrian detector for person/non-person classification.
  \item This is done by evaluating the detector only in a small region around the back-projected segmented 3D points.
  \item If the object is then continuosly tracked no further classification is performed. 
  \item To deal with occlusions the tracklet is only terminated if no ROI can be associated to a tracked object for $t_{term}$ frames.
\end{itemize}
\end{frame}

%slide 10
\subsection{Carried Items Detection}
\begin{frame}
\frametitle{Pedestrian Model} 
\begin{columns}[T]
    \begin{column}{.7\textwidth}
     \begin{itemize}
	  \item Through the tracking process the 3D model of each tracked person is constantly refined and can afterwards be used to analyze its shape and volume in detail.
	  \item The idea is to compare the online model to a learned statistical shape template of pedestrians in order to detect deviations, that cannot be explained by variation in GCT volume.
	  \item Pedestrian Model:
	  \begin{itemize}
	  	\item The model was learned by collecting a training set of 12 GCT models of pedestrians moving in different directions over a duration of 15-20 frames from a separate training sequence.
	  \end{itemize}
\end{itemize}
    \end{column}
    \begin{column}{.3\textwidth}
    	\includegraphics[width=2.5cm]{pedestian_model.jpg}
    \end{column}
\end{columns}
\end{frame}



\end{document}
